import { Controller, Get } from '@nestjs/common';
import { AppService } from './app.service';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get()
  getHello(): string {
    return this.appService.getHello();
  }

  @Get('fetch')
  getFetch(): { message: string } {
    return this.appService.getFetch();
  }

  @Get('env')
  getVarEnv(): string {
    return this.appService.getVarEnv();
  }
}
