import { Injectable } from '@nestjs/common';

@Injectable()
export class AppService {
  getHello(): string {
    return 'API formation Docker fonctionne!';
  }

  getFetch(): { message: string } {
    return { message: 'API formation Docker fonctionne depuis le front!' };
  }

  getVarEnv(): string {
    return process.env.VAR_ENV || 'Variable d environnement non définie';
  }
}
